﻿#include <iostream>
using namespace std;

class Vector
{
private:
    double x;
    double y;
    double z;
public:
    Vector()
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        cout << "Vector: " << ' ' << x << ' ' << y << ' ' << z;
    }
    float length()
    {
        return double (x * x + y * y + z * z);    
    }
};

int main()
{
    Vector v(1, 2, 3);
    v.Show();
    cout << '\n' << "Vector length: " << v.length();
}